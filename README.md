# Pt

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450"></p>

🔎 **Smart, Extensible Build Framework**

## Pasos para iniciar el proyecto

Instalacion de nx `npm install -g nx`
Instalacion de librerias para el proyecto `npm install`

## Iniciar proyecto localhost

`nx serve parrot`
El proyecto iniciara en http://localhost:4200/

## Generar build de contenido estatico para despliegue

`nx build parrot` El contenido generado se almacenara en `dist/`

## Correr los test

`nx test` Se ejecutaran los tests definidos en la carpeta `tests`

## Heroku deploy
