import React from 'react';

import '@testing-library/jest-dom/extend-expect';

import { render } from '@testing-library/react';

import LoginComponent from '../src/components/login/LoginComponent';

describe('LoginComponent', () => {
  it('should render successfully', () => {
    const { getByText } = render(<LoginComponent />);

    expect(getByText('Acceso')).toBeTruthy();
  });
});
