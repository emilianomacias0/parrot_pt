import React from 'react';

import '@testing-library/jest-dom/extend-expect';

import { render } from '@testing-library/react';

import TarjetaProducto from '../src/components/productos/TarjetaProductos';

describe('TarjetaProducto', () => {
  const product = [
    {
      uuid: '2618ec65-f996-4b12-898b-b6cf1cc32384',
      name: 'Combo Amigos',
      description:
        '2 Subs de 15 cm (elige entre Jamón de Pavo, Sub de Pollo o Atún) + 2 bebidas embotelladas de 600 ml + 2 Bolsas de papas Sabritas o 2 galletas.',
      imageUrl:
        'https://d1ralsognjng37.cloudfront.net/b49451f6-4f81-404e-bb97-2e486100b2b8.jpeg',
      legacyId: '1',
      price: '189.00',
      alcoholCount: 0,
      soldAlone: true,
      availability: 'AVAILABLE',
      providerAvailability: null,
      category: {
        uuid: 'bbc22898-7bd3-4512-8b09-64c4e19d7a9b',
        name: 'Combos Especiales',
        sortPosition: 99,
      },
    },
  ];
  it('should render successfully', () => {
    const { baseElement } = render(<TarjetaProducto products={product} />);

    expect(baseElement).toBeTruthy();
  });
  it('should have the name of the product as a title', () => {
    const { getByText } = render(<TarjetaProducto products={product} />);

    expect(getByText('Combo Amigos')).toBeTruthy();
  });
});
