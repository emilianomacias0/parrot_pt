import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import { IsLoading } from '../reducers/actions/loadingActions';
/*reducers*/
import reducer from '../reducers/index';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));
//const store = createStore(reducer, applyMiddleware(thunk));

//-- [INI] Interceptors
axios.interceptors.request.use(
  async (config) => {
    //console.log("INTERCEPTADO1...")
    store.dispatch(IsLoading({ loading: true }));
    return config;
  },
  (error) => {
    //console.log("INTERCEPTADO2...")
    store.dispatch(IsLoading({ loading: false }));
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => {
    ///console.log("INTERCEPTADO3...")
    store.dispatch(IsLoading({ loading: false }));
    return response;
  },
  (error) => {
    store.dispatch(IsLoading({ loading: false }));
    ///console.log("INTERCEPTADO4...")
    return Promise.reject(error);
  }
);
//--[FIN] Interceptors

export default store;
