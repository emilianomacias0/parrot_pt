export const BASE_URL = 'http://api-staging.parrot.rest/api/';

export const VERSION = 'v1/';

export const PRODUCTS = 'products/';

export const AUTH = 'auth/token';

export const STORES = 'users/me';

export const AVAILABILITY = '/availability';

export const HEALTH = 'auth/token/test';
