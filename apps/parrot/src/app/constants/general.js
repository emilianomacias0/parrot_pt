export const AUTHORIZATION = 'authorization';
export const TOKEN = 'token';
export const REFRESH = 'refresh';
export const STORE_UUID = 'store_uuid';
