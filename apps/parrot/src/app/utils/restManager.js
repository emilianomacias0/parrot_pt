import axios from 'axios';
import { BASE_URL, VERSION } from '../constants/endPoints';
import { TOKEN } from '../constants/general';

export default {
  connect(options) {
    const addToken = options.addToken || true;
    const noVersion = options.noVersion || false;
    const config = {
      method: options.method || 'GET',
      url: getUrl(options.url, noVersion ? '' : VERSION),
      headers: options.headers || {},
      data: options.data || null,
      responseType: options.responseType || 'json',
      params: options.params || null,
    };

    let token = sessionStorage.getItem(TOKEN);
    if (token && addToken && !validAuthorization(config.headers)) {
      config.headers.Authorization = 'Bearer ' + token;
    }
    return axios(config)
      .then((response) => {
        return {
          error: null,
          data: response.data,
          headers: response.headers,
          code: response.status,
        };
      })
      .catch((error) => {
        console.log('REST_MANAGER:::', JSON.stringify(error));
        if (error.message.includes('401') && token !== null) {
          console.log('ADD_TOKEN:::', token);
          //si el token expiro redirecciona al inicio
          redirect();
          return { error: 'Usuario no existe', data: null };
        }
        return { error: error.response.data.err, data: null };
      });
  },
};

function redirect() {
  let getUrl = window.location;
  let baseUrl =
    getUrl.protocol + '//' + getUrl.host + '/' + getUrl.pathname.split('/')[1];
  sessionStorage.clear();
  window.location.href = baseUrl;
}
const validAuthorization = (headers) => {
  if (headers && headers !== null) {
    return (
      headers.Authorization &&
      headers.Authorization !== null &&
      headers.Authorization.length > 0
    );
  }
  return false;
};

const getUrl = (ruta, version) => {
  let ep = BASE_URL + version + ruta;

  return ep;
};
