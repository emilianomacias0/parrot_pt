import { HEALTH } from '../constants/endPoints';
import { REFRESH, TOKEN } from '../constants/general';
import restManager from './restManager';

export const redirect = () => {
  if (!sessionStorage.getItem(TOKEN) || !sessionStorage.getItem(REFRESH)) {
    sessionStorage.clear();
  }
};

export const sessionValidator = () => {
  let config = {
    method: 'GET',
    url: HEALTH,
    noVersion: true,
  };
  if (sessionStorage.getItem(TOKEN) && sessionStorage.getItem(REFRESH)) {
    return new Promise((resolve, reject) => {
      restManager
        .connect(config)
        .then((response) => {
          resolve(response.data);
        })
        .catch((error) => {
          reject(JSON.stringify(error));
        });
    });
  } else {
    return new Promise((resolve, reject) => {
      reject(false);
    });
  }
};
