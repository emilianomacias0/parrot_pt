import { SESSION_AT } from "../constants/actionTypes";
const defaultStore = {
    isLogin: false
}
function reducer(state = defaultStore, actions){
    switch (actions.type) {
        case SESSION_AT: {
            return Object.assign( {}, state, {isLogin: actions.payload});
        }
        default:
			return state;
    }
}
export default reducer;