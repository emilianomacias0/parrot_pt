import { handleActions } from 'redux-actions';
import { LOAD } from './actions/storesActions';

const defaultState = {
  stores: [],
};

export default handleActions(
  {
    [LOAD](state, { payload }) {
      console.log('PAYLOAD:::', payload);
      return {
        ...state,
        stores: [...payload.data],
      };
    },
  },
  defaultState
);
