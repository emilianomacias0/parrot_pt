import { handleActions } from 'redux-actions';
import { LOAD_PRODUCTS } from './actions/productsActions';

const defaultState = {
  products: [],
};

export default handleActions(
  {
    [LOAD_PRODUCTS](state, { payload }) {
      return {
        ...state,
        products: [...payload.data],
      };
    },
  },
  defaultState
);
