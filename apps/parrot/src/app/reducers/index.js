import { combineReducers } from 'redux';

import Loading from '../reducers/loadingReducer';
import Stores from '../reducers/storesReducer';
import Products from '../reducers/productsReducer';
const reducer = combineReducers({
  loading: Loading,
  stores: Stores,
  products: Products,
});

export default reducer;
