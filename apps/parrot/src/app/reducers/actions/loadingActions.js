import { createAction } from 'redux-actions';

export const IsLoading = createAction('IsLoading');
