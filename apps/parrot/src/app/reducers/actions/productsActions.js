import { createAction } from 'redux-actions';

export const LOAD_PRODUCTS = createAction('LOAD_PRODUCTS');
