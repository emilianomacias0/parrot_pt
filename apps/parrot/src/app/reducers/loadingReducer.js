import { handleActions } from 'redux-actions';
import { IsLoading } from './actions/loadingActions';

const defaultState = {
  show: false,
};

export default handleActions(
  {
    [IsLoading](state, { payload }) {
      return {
        ...state,
        show: payload.loading,
      };
    },
  },
  defaultState
);
