import styles from './app.module.css';

import { ReactComponent as Logo } from './logo.svg';
import star from './star.svg';
import {Header} from '@pt/header';
import Main from '../components/Main';
export function App() {
  return (
    <Main/>
  );
}

export default App;
