import React from 'react';
import {
	Route,
	Switch,
	BrowserRouter
} from "react-router-dom";

import Login from'./login/Login';
import Menu from './menu/Menu';

function Main() {
return(
<BrowserRouter>
    <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/menu" component={Menu} />
    </Switch>
</BrowserRouter>
);

}

export default  Main;