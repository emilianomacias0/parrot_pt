import React, { Component } from 'react';
import { Container, FormControl, InputLabel, Select } from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
import { getProducts, getStores } from '../menu/process';
import { STORE_UUID } from '../../app/constants/general';

class SucursalesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      store: 0,
      stores: props.stores,
    };
  }

  componentDidMount() {
    getStores()
      .then((response) => {
        if (response.data.result) {
          this.setState({ stores: response.data.result.stores });
        }
      })
      .catch((err) => {
        console.log(err);
      });

    if (sessionStorage.getItem(STORE_UUID)) {
      const store = sessionStorage.getItem(STORE_UUID);
      this.setState({ store });
      getProducts(store).catch((err) => {
        console.log('ERROR:::', err);
      });
    }
  }

  onChangeHandler = (evt) => {
    const store = evt.target.value;

    this.setState({ store });
    sessionStorage.setItem(STORE_UUID, store);
    getProducts(store).catch((err) => {
      console.log('ERROR:::', err);
    });
  };
  render() {
    const { store, stores } = this.state;
    return (
      <Container>
        <FormControl>
          <InputLabel id="demo-simple-select-label">Sucursal</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={store}
            label="Stores"
            onChange={this.onChangeHandler}
          >
            <MenuItem value={0}>Elije Una sucursal</MenuItem>
            {stores &&
              stores.length > 0 &&
              stores.map((item) => {
                return (
                  <MenuItem key={item.uuid} value={item.uuid}>
                    {item.name}
                  </MenuItem>
                );
              })}
          </Select>
        </FormControl>
      </Container>
    );
  }
}

export default SucursalesComponent;
