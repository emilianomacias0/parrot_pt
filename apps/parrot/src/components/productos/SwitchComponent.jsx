import React, { useState } from 'react';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';

function SwitchComponent({ onAction, avaliable, item }) {
  const [active, setActive] = useState(
    avaliable === 'UNAVAILABLE' ? false : true
  );

  return (
    <FormGroup>
      <FormControlLabel
        control={<Switch defaultChecked={active} color="warning" />}
        label={active ? 'Desactivar' : 'Activar'}
        onChange={(evt, status) => {
          onAction(evt, item, status);
          setActive(status);
        }}
      />
    </FormGroup>
  );
}

export default SwitchComponent;
