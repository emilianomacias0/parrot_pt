import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import PropTypes from 'prop-types';
import { Grid } from '@mui/material';
import SwitchComponent from './SwitchComponent';
import { AVAILABILITY, PRODUCTS } from '../../app/constants/endPoints';
import restManager from '../../app/utils/restManager';
import { getProducts } from '../menu/process';
import { STORE_UUID } from '../../app/constants/general';

function TarjetaProductos({ products }) {
  const switchHandler = (evt, item, operation) => {
    console.log('AVALIABLE', operation);
    const type = operation ? 'AVAILABLE' : 'UNAVAILABLE';
    if (item) {
      const config = {
        url: PRODUCTS + item + AVAILABILITY,
        method: 'PUT',
        data: {
          availability: type,
        },
      };

      restManager
        .connect(config)
        .then((response) => {
          if (response && response.code === 200) {
            if (sessionStorage.getItem(STORE_UUID)) {
              const store = sessionStorage.getItem(STORE_UUID);
              getProducts(store);
            }
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return (
    <Grid container spacing={2}>
      {products &&
        products.length > 0 &&
        products.map((item) => {
          return (
            <Grid key={item.uuid} item xs={6} lg={3} className="cardItem">
              <Card sx={{ maxWidth: 345 }}>
                <CardHeader
                  avatar={
                    <Avatar
                      sx={{ bgcolor: red[500] }}
                      className={
                        item.availability === 'UNAVAILABLE' ? 'inactive' : ''
                      }
                      aria-label="recipe"
                    >
                      {item.name.charAt(0).toUpperCase()}
                    </Avatar>
                  }
                  title={item.name}
                  subheader={'Precio: $ ' + item.price}
                />
                <CardMedia
                  className={
                    item.availability === 'UNAVAILABLE' ? 'productImage' : ''
                  }
                  component="img"
                  height="194"
                  image={item.imageUrl}
                  alt={item.name}
                />
                <CardContent>
                  <Typography variant="body2" color="text.secondary">
                    {item.description}
                  </Typography>
                </CardContent>
                <CardActions disableSpacing>
                  <SwitchComponent
                    avaliable={item.availability}
                    item={item.uuid}
                    onAction={switchHandler}
                  />
                </CardActions>
              </Card>
            </Grid>
          );
        })}
    </Grid>
  );
}

TarjetaProductos.propTypes = {
  products: PropTypes.array.isRequired,
};

export default TarjetaProductos;
