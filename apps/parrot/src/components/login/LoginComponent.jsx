import React, { Component } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import restManager from '../../app/utils/restManager';
import { AUTH } from '../../app/constants/endPoints';
import { REFRESH, TOKEN } from '../../app/constants/general';
import ErrorMessage from '../errorMessage/ErrorMesage';
import { sessionValidator } from '../../app/utils/functions';

const theme = createTheme();

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loading: false,
      ready: false,
      errorMessage: '',
    };
  }

  componentDidMount() {
    sessionValidator()
      .then((response) => {
        if (response.status === 'ok') {
          const { history } = this.props;
          history.push('/menu');
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    if ((data.get('email') !== '') & (data.get('password') !== '')) {
      this.getToken(data.get('email'), data.get('password'));
    }
  };

  getToken = (email, password) => {
    const { history } = this.props;
    this.clearError();
    this.validaCampos();
    //Username android-challenge@parrotsoftware.io
    //Password 8mngDhoPcB3ckV7X

    const config = {
      url: AUTH,
      noVersion: true,
      method: 'POST',
      addToken: false,
      data: {
        email,
        password,
      },
    };
    restManager
      .connect(config)
      .then((response) => {
        if (response.error) {
          this.setError(response.error);
        } else if (response.data) {
          sessionStorage.setItem(TOKEN, response.data.access);
          sessionStorage.setItem(REFRESH, response.data.refresh);
          history.push('/menu');
        }
      })
      .catch((err) => {
        console.log('ERROR:::', err);
      });
  };

  emailHandler = (evt) => {
    const email = evt.target.value;
    const { password } = this.state;
    this.setState({ email });
    if (password !== '' && email !== '') {
      this.setState({ ready: true });
    } else {
      this.setState({ ready: false });
    }
  };

  passwordHandler = (evt) => {
    const password = evt.target.value;
    const { email } = this.state;
    this.setState({ password });
    if (password !== '' && email !== '') {
      this.setState({ ready: true });
    } else {
      this.setState({ ready: false });
    }
  };

  validaCampos = () => {
    const { email, password } = this.state;
    let errorMessage = '';
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!re.test(String(email).toLowerCase())) {
      errorMessage = 'No es un email valido';
    }

    if (password && password.length < 8) {
      errorMessage = 'La contrasena debe contener al menos 8 caracteres';
    }

    this.setError(errorMessage);
  };
  setError = (msg) => {
    this.setState({ errorMessage: msg });
  };
  clearError = () => {
    this.setState({ errorMessage: '' });
  };

  render() {
    const { errorMessage, email, password, ready } = this.state;
    return (
      <ThemeProvider theme={theme}>
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: '#f04b4a' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Acceso
            </Typography>
            <Box
              component="form"
              onSubmit={this.handleSubmit}
              noValidate
              sx={{ mt: 1 }}
            >
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Correo"
                name="email"
                autoComplete="email"
                autoFocus
                value={email}
                onChange={this.emailHandler}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Contrasena"
                type="password"
                id="password"
                autoComplete="current-password"
                value={password}
                onChange={this.passwordHandler}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                disabled={!ready}
              >
                Iniciar Sesion
              </Button>
              <Grid container>
                <Grid item xs>
                  {errorMessage !== '' && (
                    <ErrorMessage message={errorMessage} />
                  )}
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Container>
      </ThemeProvider>
    );
  }
}

export default LoginPage;
