import React, { Component } from 'react';
import LoadingModal from '../modals/LoadingModal';
import LoginPage from './LoginComponent';

class Login extends Component {
  render() {
    const { history } = this.props;
    return (
      <>
        <LoadingModal />
        <LoginPage history={history} />
      </>
    );
  }
}

export default Login;
