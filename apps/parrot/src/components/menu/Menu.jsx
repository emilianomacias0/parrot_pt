import React from 'react';

import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TopBar from '../layout/navBar/TopBar';
import { connect } from 'react-redux';
import CategoriasComponent from '../categorias/CategoriasComponent';
import SucursalesComponent from '../sucursal/SucursalComponent';

const theme = createTheme();

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: props.products,
    };
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <TopBar />
        <main>
          {/* Hero unit */}
          <Box
            sx={{
              bgcolor: 'background.paper',
              pt: 8,
              pb: 6,
            }}
          >
            <Container maxWidth="sm">
              <Typography
                component="h1"
                variant="h2"
                align="center"
                color="text.primary"
                gutterBottom
              >
                Menu
                <SucursalesComponent />
              </Typography>

              <Typography
                variant="h5"
                align="center"
                color="text.secondary"
                paragraph
              >
                Selecciona usa sucursal para obtener las categorias
              </Typography>
            </Container>

            <Container>
              {this.props.products && (
                <CategoriasComponent categories={this.props.products} />
              )}
            </Container>
          </Box>
        </main>
      </ThemeProvider>
    );
  }
}

export default connect((state) => ({
  stores: state.stores,
  products: state.products.products,
}))(Menu);
