import restManager from '../../app/utils/restManager';
import store from '../../app/store/index';
import { PRODUCTS, STORES } from '../../app/constants/endPoints';
import { LOAD_PRODUCTS } from '../../app/reducers/actions/productsActions';

export const getStores = () => {
  let config = {
    method: 'GET',
    url: STORES,
  };
  return new Promise((resolve, reject) => {
    restManager
      .connect(config)
      .then((response) => {
        // store.dispatch(LOAD({ data: response.data.result.stores }));
        resolve(response);
      })
      .catch((error) => {
        reject(JSON.stringify(error));
      });
  });
};

export const getProducts = (store_uuid) => {
  let config = {
    method: 'GET',
    url: PRODUCTS,
    params: {
      store: store_uuid,
    },
  };
  if (store_uuid === 0) {
    return new Promise((resolve, reject) => {
      store.dispatch(LOAD_PRODUCTS({ data: [] }));
      resolve([]);
    });
  }
  return new Promise((resolve, reject) => {
    restManager
      .connect(config)
      .then((response) => {
        if (response.data) {
          const { results } = response.data;
          const categories = results.map((item) => {
            return item.category;
          });

          const key = 'name';
          const unique = [
            ...new Map(categories.map((item) => [item[key], item])).values(),
          ];

          const productsByCategory = [];
          for (let i = 0; i < unique.length; i++) {
            const products = results.filter(
              (item) => item.category.name === unique[i].name
            );
            let category = { ...unique[i], products: [...products] };
            productsByCategory.push(category);
          }
          store.dispatch(LOAD_PRODUCTS({ data: productsByCategory }));
          resolve(productsByCategory);
        }
      })
      .catch((error) => {
        console.log('ALGO_SUCEDIO', error);
        reject(JSON.stringify(error));
      });
  });
};
