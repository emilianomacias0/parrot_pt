import React from 'react';
import { connect } from 'react-redux';
import './style.sass';
import LoadingOverlay from 'react-loading-overlay';

const LoadingModal = ({ ldl }) => {
  return (
    <>
      {ldl.show && (
        <div className="oneMomentPlease">
          <LoadingOverlay
            className="loading"
            active={true}
            spinner
            text="Cargando..."
          ></LoadingOverlay>
        </div>
      )}
      <span></span>
    </>
  );
};

export default connect((state) => ({
  ldl: state.loading,
}))(LoadingModal);
