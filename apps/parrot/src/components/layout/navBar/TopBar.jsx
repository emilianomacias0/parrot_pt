import React, { Component } from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import { withRouter } from 'react-router';
import LoadingModal from '../../modals/LoadingModal';
import { REFRESH, TOKEN } from '../../../app/constants/general';

class TopBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      auth: true,
      anchorEl: null,
    };
  }
  componentDidMount() {
    this.redirect();
  }
  componentDidUpdate() {
    this.redirect();
  }
  redirect = () => {
    if (!sessionStorage.getItem(TOKEN) || !sessionStorage.getItem(REFRESH)) {
      const { history } = this.props;
      sessionStorage.clear();
      history.push('/');
    }
  };

  handleChange = (event) => {
    this.setAuth(event.target.checked);
  };

  handleMenu = (event) => {
    this.setAnchorEl(event.currentTarget);
  };

  handleClose = () => {
    const { history } = this.props;
    sessionStorage.clear();
    history.push('/');
  };
  setAnchorEl = (state) => {
    this.setState({ anchorEl: state });
  };

  setAuth = (auth) => {
    this.setState({ auth });
  };

  render() {
    const { anchorEl } = this.state;
    return (
      <>
        <LoadingModal />
        <Box sx={{ flexGrow: 1 }}>
          <AppBar position="static" color="warning" className="prrotAppBar">
            <Toolbar>
              <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                PARROT
              </Typography>
              <div>
                <IconButton
                  size="large"
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={Boolean(anchorEl)}
                  onClose={this.handleClose}
                >
                  <MenuItem onClick={this.handleClose}>Cerrar Sesion</MenuItem>
                </Menu>
              </div>
            </Toolbar>
          </AppBar>
        </Box>
      </>
    );
  }
}

export default withRouter(TopBar);
