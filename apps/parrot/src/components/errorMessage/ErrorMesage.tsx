import React from 'react';
import PropTypes from 'prop-types'; 


function ErrorMessage({message}){

    return <span className="errorMessage">{message}</span>
}

ErrorMessage.propTypes = {
	message: PropTypes.string.isRequired
}

export default ErrorMessage;